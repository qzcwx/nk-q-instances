import sys
sys.path.append('../')
import nkLandscape as nk
import nkqLandscape as nkq
import random as random
import numpy as np

# sys.path.append('/home/chenwx/workspace/SumSat/')

# iRange = range(10)
iRange = range(1)

rseed = 0

nRange = [20,50,100,150,200,250,300,350,400,450,500]

cv = [1]

kRange = [2,4]


cRange = list( np.array(cv) * np.array(nRange))
cRange = [int(i) for i in cRange]         # convert float to int

tRange = [4]
qRange = [2]

prob = 'NKQ'

prefixNK = './NK/'
prefixNKQ = './NKQ/'

for n in nRange:
    for k in kRange:
        c = cRange[nRange.index(n)]
        # for c in cRange:
        for t in tRange:
            for i in iRange:
                print 'N', n, 'K', k, 'I', i
                random.seed(rseed+i)
                model = nk.NKLandscape(n,k,c)
                if prob == 'NK':
                    # NK landscapes:
                    model.exportToFile(prefixNK+'NK-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i))
                elif prob == 'NKQ':
                    # NKq landscapes:
                    for q in qRange:
                        model = nkq.NKQLandcape(n, k, c, q, t)
                        model.exportToFile(prefixNKQ+'NKQ-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i)+'-Q'+str(q)+'-T'+str(t))
