# generate spin glasses model

import os
import subprocess

generator = "/home/chenwx/workspace/inst/rudy/rudy"
outDir = "/home/chenwx/workspace/SumSat/benchmark/spin"
optRange = ['2g']

# grid = [(10,10), (15,15), (20, 20), (25, 25), (30,30), ()]
gRange = [5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100]
grid = [(i,i) for i in gRange]

iRange = range(1)
# os.system(genPath)
for i in iRange:
    for g in grid:
        for opt in optRange:
            # print '%s/spin%s-%dX%d-I%d' %(outDir,opt,g[0],g[1],i)
            with open('%s/spin%s-%dX%d-I%d' %(outDir,opt,g[0],g[1],i), 'w') as out:
                # print '%s -spinglass%s %d %d %d' %(generator,opt, g[0],g[1],i)
                return_code = subprocess.call("%s -spinglass%s %d %d %d" %(generator,opt, g[0],g[1],i), shell=True, stdout = out)

