import sys
sys.path.append('../')
import nkLandscape as nk
import nkqLandscape as nkq
import random as random
import numpy as np

# sys.path.append('/home/chenwx/workspace/SumSat/')

iRange = range(1)

rseed = 0

# nRange = [20,50,100,200,300,400,500]
# nRange = [300,400]
# nRange = [150,250,350,450]
# nRange = [10]
nRange = [20,50,100,150,200,250,300,350,400,450,500]

cv = [1]

kRange = [2,4]
# kRange = [2]
# kRange = [6]

cRange = nRange

tRange = [4]
qRange = [2]

prob = 'NonNKQ'

prefixNK = './NK/'
prefixNKQ = './NKQ/'

for n in nRange:
    for k in kRange:
        # for c in cRange:
        c = cRange[nRange.index(n)]
        for t in tRange:
            for i in iRange:
                random.seed(rseed+i)
                if prob == 'NK':
                    # NK landscapes:
                    model = nk.NKLandscape(n,k,c)
                    model.exportToFile(prefixNK+prob+'-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i))
                elif prob == 'NKQ':
                    # NKq landscapes:
                    for q in qRange:
                        model = nkq.NKQLandcape(n, k, c, q, t)
                        model.exportToFile(prefixNKQ+prob+'-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i)+'-Q'+str(q)+'-T'+str(t))
                elif prob == 'NonNK':
                    model = nk.NonNKLandscape(n, k, c)
                    # print 'out', model.neighs
                    model.exportToFile(prefixNK+prob+'-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i))
                    # print 'end', model.neighs
                elif prob == 'NonNKQ':
                    for q in qRange:
                        model = nkq.NonNKQLandscape(n, k, c, q, t)
                        model.exportToFile(prefixNKQ+prob+'-N'+str(n)+'-K'+str(k)+'-C'+str(c)+'-I'+str(i)+'-Q'+str(q)+'-T'+str(t))
